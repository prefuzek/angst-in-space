(ns angst.library.textinput
  (:require [quil.core :as q]
            [angst.library.utils :refer :all]
            [angst.library.actionlog :as log]
            [angst.library.effects :as e]
            [angst.library.network :as network]
            [angst.library.turn :as turn]))

(declare commands)

(defn draw-text-input
  [{:keys [rect-mode x y width height text-align
           text-offset-x text-offset-y value]
    :as input}]
  (if input
    (do (q/no-fill)
        (q/rect-mode rect-mode)
        (q/stroke-weight 2)
        (q/rect (scalex x) (scaley y) (scalex width) (scaley height))
        (set-fill "White")
        (q/text-align text-align)
        (q/text (or value "")
                (scalex (+ x text-offset-x))
                (scaley (+ y text-offset-y))))))

(defn update-chat-online [state]
  (condp = (:online-state state)
    :host (do-effects state e/effects [[:write-server-data]])
    :client (do (network/send-new-state state (network/get-address state)) state)
    :offline state))

(defn do-command [state command]
  (let [command-words (clojure.string/split (subs command 1) #" ")]
    (try 
      (log/add-log-entry 
       (apply ((keyword (first command-words)) commands)
              (cons state (rest command-words)))
       :use-command
       (get-player-name state)
       (clojure.string/join " " command-words))
      (catch Exception e 
        (clojure.stacktrace/print-stack-trace e)
        (log/add-log-entry state :command-error
                           (get-player-name state)
                           (clojure.string/join " " command-words))))))

(defn process-message [state message]
  (if (= (first message) \\)
    (do-command state message)
    (log/add-log-entry state :chat (get-player-name state) message)))

(defn send-chat-message [state]
  (-> state
      (process-message (-> state :text-inputs :chat-input :value))
      (assoc-in [:text-inputs :chat-input :value] "")
      (update-chat-online)))

(defn sanitize [s]
  (apply str (filter #(<= 20 (int %) 126) s)))

(defn add-char [state input raw-key]
  (update-in state [:text-inputs input :value]
             #(if (< (count %) (-> state :text-inputs input :max-length))
                (sanitize (str % raw-key))
                %)))

(defn update-text-input [state]
  (let [raw-key (char (q/raw-key))
        active-input (peek (:active-text-input state))]
    (if active-input
      (cond (= raw-key \u0008) ;Backspace
            (update-in state [:text-inputs active-input :value]
                       #(if (> (count %) 0) (subs % 0 (dec (count %)))))
            (and (= (int raw-key) 10) (= active-input :chat-input)) ; Enter
            (send-chat-message state)
            :else
            (add-char state active-input raw-key))
      state)))

(defn validate-planet [state planet]
  (when-not (some #{(keyword planet)} (keys (:planets state)))
    (throw (Exception. (str planet " is not a planet")))))

(defn validate-empire [state empire]
  (when-not (some #{(keyword empire)} (keys (:empire state)))
    (throw (Exception. (str empire " is not an empire")))))

(def commands
  {:join-game
   (fn [state]
     (do-effects state e/effects [[:remove-gcomponent :all]
                                  [:add-gcomponent :map]
                                  [:add-gcomponent :infobar]]))

   :add-resources
   (fn [state empire resources]
     (validate-empire state empire)
     (update-empire-value state (keyword empire) :resources
                          #(+ % (Integer/parseInt resources))))

   :set-resources
   (fn [state empire resources]
     (validate-empire state empire)
     (set-empire-value state (keyword empire) :resources
                       (Integer/parseInt resources)))

   :add-vp
   (fn [state empire vp]
     (validate-empire state empire)
     (update-empire-value state (keyword empire) :vp (Integer/parseInt vp)))

   :set-vp
   (fn [state empire vp]
     (validate-empire state empire)
     (set-empire-value state (keyword empire) :vp (Integer/parseInt vp)))

   :set-progress
   (fn [state planet progress]
     (validate-planet state planet)
     (set-planet-value state (keyword planet) :progress
                       (Integer/parseInt progress)))

   :set-ships
   (fn [state planet ships]
     (validate-planet state planet)
     (set-planet-value state (keyword planet) :ships
                       (max 0 (Integer/parseInt ships))))

   #_(:set-ship-colour
   (fn [state planet colour]
     (validate-planet state planet)
     (set-planet-value state (keyword planet) :ship-colour colour)))

   :set-development
   (fn [state planet development]
     (validate-planet state planet)
     (set-planet-value state (keyword planet) :development
                       (max -1 (Integer/parseInt development))))

   :set-empire
   (fn [state planet empire]
     (println (-> state :empire ((keyword empire)) :colour))
     (let [colour (or (-> state :empire ((keyword empire)) :colour) "Black")]
       (validate-planet state planet)
       (-> state (set-planet-value (keyword planet) :colour colour)
           (set-planet-value (keyword planet) :ship-colour colour))))

   :destroy-planet
   (fn [state planet]
     (validate-planet state planet)
     (-> state
         (update-in [:planets (keyword planet)]
                    #(merge % {:colour "Black"
                               :ship-colour "Black"
                               :ships 0
                               :moved 0
                               :development -1}))
         (remove-effects (keyword planet))
         (#(if (get-in % [:planets (keyword planet) :project])
             (update-in % [:planets (keyword planet)]
                        (fn [p]
                          (merge p {:project "inactive" :progress 0})))
             %))))

   :set-planet-used
   (fn [state planet used?]
     (set-planet-value state (keyword planet) :used
                       (Boolean/valueOf used?)))

   :end-turn
   (fn [state] (turn/end-turn state))})
