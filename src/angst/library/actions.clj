(ns angst.library.actions
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [angst.library.actionlog :as log]
            [angst.library.effects :as e]
            [angst.library.hooks :as hooks]
            [angst.library.turn :refer :all]
            [angst.library.utils :refer :all]))

(defn connected?
  "Checks if two planets are connected"
  [state planet1 planet2]
  (member? planet1 (-> state :planets planet2 :connections)))

(defn colonize [{:keys [active planets] :as state} planet]
  (if (and (= (-> planets planet :colour) "Black")
           (> (-> planets planet :ships) 0)
           (= (-> planets planet :ship-colour) (:colour (empire state)))
           (> (:resources (empire state)) 2))

    (-> state
        (update-planet-value planet :ships dec)
        (set-planet-value planet :colour (:colour (empire state)))
        (set-planet-value planet :used true)
        (log/add-log-entry :colonize active planet)
        (update-in [:empire active :resources]
                   #(- % (col-cost state planet))))
    state))

(defn get-resources [{:keys [active planets] :as state} planet]
  (let [{:keys [production development]} (planet planets)
        resources (get production development)]
    (-> state
        (update-empire-value active :resources #(+ % resources))
        (log/add-log-entry :produce-resources active planet resources)
        (end-project planet))))

(defn reset-moved [state]
  "Sets all ships to unmoved"
  (reduce #(set-planet-value %1 %2 :moved 0) state (keys (:planets state))))

(defn begin-command
  "Sets active-planet to a planet key, updates the button, and
  sets all :used to 0"
  [state planet]
  (-> state
      (assoc :active-planet planet)
      (change-buttons [:end-phase] [:done-command])
      (reset-moved)
      (log/add-log-entry :begin-command (:active state) planet)
      (end-project planet)))

(defn begin-move
  "Sets :ship-move to {:planet x :ships 1} and updates the button"
  [{:keys [planets active-planet effect-details] :as state} planet]
  (if (and (= (-> planets planet :ship-colour) (:colour (empire state)))
           (> (- (-> planets planet :ships)
                 (-> planets planet :moved)) 0)
           (or (connected? state planet active-planet)
               (= planet active-planet) (effect-active? state :Petiska))
           (not (and (effect-active? state :Kazo)
                     (= planet (:Kazo effect-details)))))

    (-> state (assoc-in [:effects :ship-move] {:planet planet :ships 1})
        (change-buttons [:done-command] [:cancel-move]))
    state))

(defn safe-move
  [{:keys [active planets] :as state} to-planet from-planet ship-num distance]
  (-> state
      (update-empire-value active :resources #(- % (move-cost state distance)))
      (update-planet-value from-planet :ships #(- % ship-num))
      (update-planet-value to-planet :ships #(+ % ship-num))
      (update-planet-value to-planet :moved #(+ % ship-num))
      (set-planet-value to-planet :ship-colour
                        (-> planets from-planet :ship-colour))
      (log/add-log-entry :safe-move active ship-num from-planet to-planet)
      (assoc-in [:effects :ship-move] false)
      (change-buttons [:cancel-move] [:done-command])))

(defn conquer
  [{active :active :as state} planet colour surviving]
  (-> state
      (log/add-log-entry :conquer active planet)
      (add-points active 1 (= (:major (empire state)) "Warlords"))
      (add-points active 3 (= (:major (empire state)) "Conquistadores"))
      (add-points active 2 (= (:major (empire state)) "Slavers"))
      (add-points (get-planet-empire state planet) -2
                  (= (:major (empire state)) "Slavers"))
      (update-in [:planets planet]
                 #(merge % {:ships surviving
                            :moved surviving
                            :colour colour
                            :ship-colour colour
                            :used true}))
      (end-project planet)
      (update-planet-value planet :development #(max 0 (- % 3)))
      (hooks/resolve-hooks :on-conquer {:planet planet})))

(defn resolve-attack
  [state to-planet to-info from-info surviving active-player]
  (cond (and (not= (:colour to-info) "Black") (> surviving 0))
        (-> state
            (add-points active-player (inc (:ships to-info))
                        (= (:major (empire state)) "Warlords"))
            (conquer to-planet (:ship-colour from-info) surviving))
        (> surviving 0)
        (-> state
            (add-points active-player (:ships to-info)
                        (= (:major (empire state)) "Warlords"))
            (set-planet-value to-planet :ships surviving)
            (set-planet-value to-planet :ship-colour (:ship-colour from-info))
            (set-planet-value to-planet :moved surviving))
        :else
        (-> state
            (log/add-log-entry :unsuccessful-attack (:active state) to-planet)
            ;(add-points active-player (-
            (set-planet-value to-planet :ships (max 0 (- (inc surviving)))))))

(defn attack-move
  [{active :active :as state} to-planet from-planet ship-num
   {from-ships :ships :as from-info}
   {to-ships :ships to-colour :colour :as to-info} distance]
  (let [surviving (if (= to-colour "Black")
                    (- ship-num to-ships)
                    (- ship-num (inc (planet-defense state to-planet))))]

    (-> state
        (log/add-log-entry :attack-move active
                           ship-num from-planet to-planet to-ships)
        (hooks/resolve-hooks :on-attack {:to-colour to-colour})
        (resolve-attack to-planet to-info from-info surviving active)
        #_(add-points active ship-num
                    (= (:major (empire state)) "Warlords")) ; FIXME!
        (update-planet-value from-planet :ships #(- % ship-num))
        (update-empire-value active :resources
                             #(- % (move-cost state distance)))
        (check-shoran ship-num)
        (assoc-in [:effects :ship-move] false)
        (change-buttons [:cancel-move] [:done-command]))))

(defn continue-move [{:keys [planets effects] :as state} to-planet]
  (let [from-planet (:planet (:ship-move effects))
        ship-num (-> state :effects :ship-move :ships)
        {to-colour :colour to-ship-colour :ship-colour to-ships :ships
         to-moved :moved to-x :x to-y :y :as to-info}
        (to-planet planets)
        {from-ship-colour :ship-colour from-x :x from-y :y :as from-info}
        (from-planet planets)
        distance (get-distance from-x from-y to-x to-y)]
    (println to-planet from-planet)

    (if (= to-planet from-planet)

      (if (> (- to-ships to-moved) ship-num)
        (update-in state [:effects :ship-move :ships] inc)
        state)

      (if (and (connected? state to-planet from-planet)
               (>= (:resources (empire state)) (move-cost state distance)))

        (if (> to-ships 0)

          (if (= to-ship-colour from-ship-colour)
            (safe-move state to-planet from-planet ship-num distance)
            (do (println 1 to-ship-colour from-ship-colour)
                (attack-move state to-planet from-planet ship-num
                         from-info to-info distance)))

          (if (or (= to-colour "Black")
                  (= to-colour from-ship-colour))
            (safe-move state to-planet from-planet ship-num distance)
            (do (attack-move state to-planet from-planet ship-num
                         from-info to-info distance))))
        state))))

(defn build-ship [state planet]
  (if (> (:resources (empire state)) 1)
    (-> state 
        (update-in [:empire (:active state) :resources] #(- % 2))
        (update-in [:planets planet :ships] inc)
        (log/add-log-entry :build-ship (:active state) planet)
        (end-project planet))
    state))

(defn start-progress
  "Adds proper amount of progress when a project starts"
  [state planet]
  (if (not (effect-active? state :Chiu))
    (add-progress state planet 1)
    (add-progress state planet (inc (quot (-> state :planets :Chiu :progress) 2)))))

(defn start-project
  "Adds planet to project effects list, sets it to active, and
  adds initial progress"
  [state planet]
  (-> state
      (update-in [:constant-effects :projects] #(vec (cons planet %)))
      (assoc-in [:planets planet :project] "active")
      (start-progress planet)
      (log/add-log-entry :start-project (:active state) planet
                         (-> state :planets planet :progress))))
