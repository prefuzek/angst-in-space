(ns angst.library.hooks)


;; Hooks are stored under an :event keyword.  A hook is
;; a map with a :function and, optionally, some :params keys
;; and a :remove hook event. When the hook is applied, it
;; will take arguments with matching param keys from the
;; (optional) arg-map supplied to resolve-hooks. Hooks are
;; removed whenever the :remove hook event is resolved.

(defn add-hook
  ([state event function]
   (add-hook state event function {} nil))
  ([state event function params]
   (add-hook state event function params nil))
  ([state event function params remove-event]
   (let [hook {:function function :params params :remove remove-event}]
     (update-in state [:hooks event]
                #(if % (conj % hook) [hook])))))

(defn apply-hooks
  [state hooks arg-map]
  (letfn [(args [hook] (map #(%1 arg-map) (:params hook)))]
    (reduce #(apply (:function %2) %1 (args %2)) state hooks)))

;; TODO: make removing neater

(defn- remove-event-hooks
  [new-hooks event event-hooks remove-event]
  (assoc new-hooks event (filterv #(not= (:remove %) remove-event) event-hooks)))

(defn- remove-hooks
  [state event]
  (update state
          :hooks
          (fn [hooks]
            (reduce-kv #(remove-event-hooks %1 %2 %3 event) {} hooks))))

(defn resolve-hooks
  ([state event]
   (resolve-hooks state event {}))
  ([state event arg-map]
   (-> state
       (apply-hooks (get-in state [:hooks event]) arg-map)
       (remove-hooks event))))
