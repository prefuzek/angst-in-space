(ns angst.library.graphics
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [angst.library.utils :refer :all]
            [angst.library.data :refer :all]
            [angst.library.gcomponents :as c]
            [angst.library.textinput :as input]))

(defn draw-buttons [state component]
  (q/fill 0 0 255) ;White
  (q/rect-mode :center)
  (q/text-align :center)
  (q/stroke-weight 2)
  (q/no-fill)
  (let [component-buttons
        (select-keys (:buttons state)
                     (into (:buttons (component c/components))
                           (:hidden-buttons (component c/components))))]
    (doseq [{:keys [x y width height label]} (map second (vec component-buttons))]
      (q/rect (scalex x) (scaley y) (scalex width) (scaley height) 10)
      (q/text label (scalex x) (scaley y)))
    (q/fill 0 0 255)
    (doseq [{:keys [x y label]} (map second (vec component-buttons))]
      (q/text label (scalex x) (scaley y)))))

(defn draw [{:keys [components text-inputs] :as state}]
  (q/background 0)
  (doseq [component components]
    (let [{:keys [draw-fn text-input]} (component c/components)]
      (draw-fn state)
      (draw-buttons state component)
      (when text-input
        (input/draw-text-input (text-input text-inputs))))))
